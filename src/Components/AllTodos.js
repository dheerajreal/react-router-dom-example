import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Loading from "./Loading"

const Todo = (props) => {
  const [Todos, setTodos] = React.useState(null);
  React.useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/todos/`)
      .then((data) => setTodos(data.data));
  }, []);


  return (
    <div>
      <p>all todos</p>
      {Todos
        ? Todos.map((item) => (
            <Link to={`${item.id}`} key={item.id}>
              <p>{item.title}</p>
            </Link>
          ))
        : <Loading/>}
    </div>
  );
};

export default Todo;
