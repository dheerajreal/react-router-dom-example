import React from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import Loading from "./Loading"

const Todo = (props) => {
  const id = props.match.params.id;
  const [Todo, setTodo] = React.useState(null);
  const history = useHistory();

  React.useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/todos/${id}`)
      .then((data) => setTodo(data.data));
  }, [id]);

  return (
    <div className="py-3 mt-3 card text-center">
      <div className="card-body">
        {Todo ? (
          <>
            <h2 className="card-title">
              {Todo.id} : {Todo.title}
            </h2>
            <p>
              {Todo.completed ? (
                <b className="text-success">Done </b>
              ) : (
                <b className="text-warning">Pending</b>
              )}
            </p>
          </>
        ) :<Loading/>}
        <button className="btn btn-primary" onClick={history.goBack}>
          Go back
        </button>
      </div>
    </div>
  );
};

export default Todo;
