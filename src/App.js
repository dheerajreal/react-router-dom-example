import { React } from "react";
import "bootstrap/dist/css/bootstrap.css";
// Put any other imports below so that CSS from your
// components takes precedence over default styles.

import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Todo from "./Components/Todo";
import AllTodos from "./Components/AllTodos";

function App() {
  return (
    <div className="container">
      <Router>
        <h1 className="mt-2 mb-4 text-center">
          <Link to="/" style={{textDecoration:"none"}}>Router-example</Link> 
        </h1>
        <hr />
        <Switch>
          <Route component={AllTodos} exact path="/"></Route>
          <Route path="/:id" component={Todo}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
